import { compress, decompress } from './compression';

it('correctly compresses a string', () => {
  expect(compress('aabbbcccccaa')).toBe('a2b3c5a2');
});


it('correctly decompresses a string', () => {
  expect(decompress('a2b3c5a2')).toBe('aabbbcccccaa');
});
