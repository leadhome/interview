Implement a simple HackerNews user interface

You can retrieve a list of the current top stories at https://hacker-news.firebaseio.com/v0/topstories.json
You can retrieve the details of an individual story at https://hacker-news.firebaseio.com/v0/item/${id}.json

List the top stories with their titles and allow a user to delete a story from the list if they don't want to see it anymore.